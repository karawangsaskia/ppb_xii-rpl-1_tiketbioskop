import 'package:flutter/material.dart';
import 'package:tiketbioskop/widgets/CustomNavBar.dart';
import 'package:tiketbioskop/widgets/NewMoviesWidget.dart';
import 'package:tiketbioskop/widgets/UpcomingWidget.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(
                  vertical: 18,
                  horizontal: 10,
                ),
                child: Row(
                  children: [
                    Column(
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.location_on, // Icon lokasi
                                color: Colors.black,
                            ),
                          ],
                        ),
                        Text(
                          "Karawang",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(width: 16), // Spacer
                    Expanded(
                      child: TextField(
                        decoration: InputDecoration(
                          hintText: " Cari tiket...",
                          hintStyle: TextStyle(
                            color: Colors.grey,
                          ),
                          border: OutlineInputBorder(),
                          contentPadding: EdgeInsets.symmetric(vertical: 12),
                        ),
                      ),
                    ),
                    IconButton(
                      icon: Icon(Icons.search),
                      onPressed: () {
                        // Implement your search functionality here
                      },
                    ),
                  ],
                ),
              ),
              // Add other widgets below as needed
              SizedBox(height: 30), // Fixed typo here
              UpcomingWidget(),
              SizedBox(height: 40),
              NewMoviesWidget(),
            ],
          ),
        ),
      ),
      bottomNavigationBar: CustomNavbar(),
    );
  }
}
