import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tiketbioskop/widgets/CustomNavBar.dart';

class CategoryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.arrow_back,
                        color: Colors.black,
                        size: 30,
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      width: double.infinity,
                      height: 200,
                      color: Colors.grey,
                      child: Align(
                        alignment: Alignment.centerLeft, // Pindahkan teks ke sisi kiri
                        child: Padding(
                          padding: const EdgeInsets.only(left: 16.0), // Padding di sisi kiri teks
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Pilih Film",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                ),
                              ),
                              SizedBox(height: 10,),
                              Align(
                                alignment: Alignment.center,
                                  child: SizedBox(
                                    width: 480, // Lebar kotak
                                    height: 50, // Tinggi kotak
                                  child: Container(
                                    color: Colors.white, // Warna latar belakang putih
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Icon(
                                        Icons.keyboard_arrow_down,
                                        color: Colors.black,
                                        ),
                                          Text(
                                           "Ant-Man",
                                          style: TextStyle(
                                          color: Colors.black,
                                            fontSize: 16,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ]
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      width: double.infinity,
                      height: 200,
                      color: Color.fromARGB(255, 154, 185, 199),
                      child: Align(
                        alignment: Alignment.centerLeft, // Pindahkan teks ke sisi kiri
                        child: Padding(
                          padding: const EdgeInsets.only(left: 16.0), // Padding di sisi kiri teks
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Pilih Kelas",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                ),
                              ),
                              SizedBox(height: 10,),
                              Align(
                                alignment: Alignment.center,
                                  child: SizedBox(
                                    width: 480, // Lebar kotak
                                    height: 50, // Tinggi kotak
                                  child: Container(
                                    color: Colors.white, // Warna latar belakang putih
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Icon(
                                        Icons.keyboard_arrow_down,
                                        color: Colors.black,
                                        ),
                                          Text(
                                           "Regular",
                                          style: TextStyle(
                                          color: Colors.black,
                                            fontSize: 16,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ]
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      width: double.infinity,
                      height: 200,
                      color: Colors.grey,
                      child: Align(
                        alignment: Alignment.centerLeft, // Pindahkan teks ke sisi kiri
                        child: Padding(
                          padding: const EdgeInsets.only(left: 16.0), // Padding di sisi kiri teks
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Jumlah Tiket",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                ),
                              ),
                              SizedBox(height: 10,),
                              Align(
                                alignment: Alignment.center,
                                  child: SizedBox(
                                    width: 480, // Lebar kotak
                                    height: 50, // Tinggi kotak
                                  child: Container(
                                    color: Colors.white, // Warna latar belakang putih
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    ),
                                  ),
                                ),
                              ),
                            ]
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      width: double.infinity,
                      height: 200,
                      color:  Color.fromARGB(255, 154, 185, 199),
                      child: Align(
                        alignment: Alignment.centerLeft, // Pindahkan teks ke sisi kiri
                        child: Padding(
                          padding: const EdgeInsets.only(left: 16.0), // Padding di sisi kiri teks
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Total Harga",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                ),
                              ),
                              SizedBox(height: 10,),
                              Align(
                                alignment: Alignment.center,
                                  child: SizedBox(
                                    width: 480, // Lebar kotak
                                    height: 50, // Tinggi kotak
                                  child: Container(
                                    color: Colors.white, // Warna latar belakang putih
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    ),
                                  ),
                                ),
                              ),
                            ]
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      width: double.infinity,
                      height: 200,
                      color: Colors.grey,
                      child: Align(
                        alignment: Alignment.centerLeft, // Pindahkan teks ke sisi kiri
                        child: Padding(
                          padding: const EdgeInsets.only(left: 16.0), // Padding di sisi kiri teks
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Via Pembayaran",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                ),
                              ),
                              SizedBox(height: 10,),
                              Align(
                                alignment: Alignment.center,
                                  child: SizedBox(
                                    width: 480, // Lebar kotak
                                    height: 50, // Tinggi kotak
                                  child: Container(
                                    color: Colors.white, // Warna latar belakang putih
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Icon(
                                        Icons.keyboard_arrow_down,
                                        color: Colors.black,
                                        ),
                                          Text(
                                           "Mandiri",
                                          style: TextStyle(
                                          color: Colors.black,
                                            fontSize: 16,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ]
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 20),
                    ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text('Kembali'),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: CustomNavbar(),
    );
  }
}
