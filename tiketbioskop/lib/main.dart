import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tiketbioskop/pages/HomePage.dart';
import 'package:tiketbioskop/pages/CategoryPage.dart';
import 'package:tiketbioskop/pages/MoviePage.dart';

void main(){
  runApp(MyApp());
}

class MyApp extends StatefulWidget{
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp>{

  @override
  void initState(){
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersiveSticky);

    super.initState();
  }

  @override
  Widget build(BuildContext context){
    return MaterialApp(
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
      scaffoldBackgroundColor: Color(0xFFFFFFFF),
    ),
    routes: {
      "/" : (context) => HomePage(),
      "CategoryPage" : (context) => CategoryPage(),
      "MoviePage" : (context) => MoviePage(),
    },
    );
  }
}